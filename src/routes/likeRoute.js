import express from "express";
import {
  getLikeByRes,
  getLikeByUser,
  likeRes,
  unLikeRes,
} from "../controllers/likeController.js";
const likeRoute = express.Router();

likeRoute.post("/like-res", likeRes);
likeRoute.post("/unlike-res", unLikeRes);
likeRoute.get("/get-like-user", getLikeByUser);
likeRoute.get("/get-like-res", getLikeByRes);
export default likeRoute;
