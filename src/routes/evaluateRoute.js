import express from "express";
import {
  addRate,
  getRateByRes,
  getRateByUser,
} from "../controllers/evaluateController.js";
const evaluteRoute = express.Router();
evaluteRoute.post("/add-rate", addRate);
evaluteRoute.get("/get-rate-by-user", getRateByUser);
evaluteRoute.get("/get-rate-by-res", getRateByRes);

export default evaluteRoute;
