import express from "express";
import likeRoute from "./likeRoute.js";
import evaluteRoute from "./evaluateRoute.js";
import orderRoute from "./orderRoute.js";
const rootRoute = express.Router();

rootRoute.use("/like", likeRoute);
rootRoute.use("/rate", evaluteRoute);
rootRoute.use("/order", orderRoute);
export default rootRoute;
