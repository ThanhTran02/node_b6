import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";

const model = initModels(sequelize);

const orderFood = async (req, res) => {
  const { user_id, food_id, amount, code, arr_sub_id } = req.body;
  await model.orders.create({ user_id, food_id, amount, code, arr_sub_id });
  res.send("Đặt món thành công");
};

export { orderFood };
