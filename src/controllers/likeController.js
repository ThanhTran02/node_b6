import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";
const model = initModels(sequelize);

const getLikeByRes = async (req, res) => {
  const { res_id } = req.body;
  let data = await model.like_res.findAll({
    where: {
      res_id,
    },

    // include: "restaurant",
  });
  res.send(data);
};

const getLikeByUser = async (req, res) => {
  const { user_id } = req.body;
  let data = await model.like_res.findAll({
    where: {
      user_id,
    },
    // include: "user ",
  });
  res.send(data);
};

const likeRes = async (req, res) => {
  let { user_id, res_id, date_like } = req.body;
  await model.like_res.create({ user_id, res_id, date_like });
  res.send("Like thành công");
};

const unLikeRes = async (req, res) => {
  let { user_id, res_id } = req.body;
  await model.like_res.update(
    { status: 0 },
    {
      where: {
        user_id,
        res_id,
      },
    }
  );
  res.send("UnLike thành công");
};
export { likeRes, unLikeRes, getLikeByRes, getLikeByUser };
