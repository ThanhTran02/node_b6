import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";

const model = initModels(sequelize);

const addRate = async (req, res) => {
  const { user_id, res_id, amount, date_rate } = req.body;
  await model.rate_res.create({ user_id, res_id, amount, date_rate });
  res.send("Thêm đánh giá thành công");
};
const getRateByUser = async (req, res) => {
  const { user_id } = req.body;
  let data = await model.rate_res.findAll({
    where: {
      user_id,
    },
    include: "user",
  });
  res.send(data);
};
const getRateByRes = async (req, res) => {
  const { res_id } = req.body;
  let data = await model.rate_res.findAll({
    where: {
      res_id,
    },
    include: "restaurant",
  });
  res.send(data);
};

export { getRateByRes, getRateByUser, addRate };
