import { Model, Sequelize } from "sequelize";

export default class RateRes extends Model {
  static init(sequelize, DataTypes) {
    super.init(
      {
        user_id: {
          type: DataTypes.INTEGER,
          allowNull: true,
          references: {
            model: "user",
            key: "user_id",
          },
        },
        res_id: {
          type: DataTypes.INTEGER,
          allowNull: true,
          references: {
            model: "restaurant",
            key: "res_id",
          },
        },
        amount: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        date_rate: {
          type: DataTypes.DATEONLY,
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: "rate_res",
        timestamps: false,
        indexes: [
          {
            name: "user_id",
            using: "BTREE",
            fields: [{ name: "user_id" }],
          },
          {
            name: "res_id",
            using: "BTREE",
            fields: [{ name: "res_id" }],
          },
        ],
      }
    );

    this.removeAttribute("id");

    return this;
  }
}
